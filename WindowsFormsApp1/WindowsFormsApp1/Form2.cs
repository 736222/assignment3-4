﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2(Double distance,string from,string to,int rad)
        {
            InitializeComponent();

            double fare = distance * 0.81;
            double total = 2.5 + fare +1.75;
            if(rad==2)
            {
                total = 2.5 + (0.1 * 2.5) + fare + (fare * 0.15) + 1.75;
            }
            TimeSpan time = DateTime.Now.TimeOfDay;
            if(( time>= new TimeSpan(10,0,0)) && (time <= new TimeSpan(12, 0, 0)) || 
                (time >= new TimeSpan(16, 0, 0)) && (time <= new TimeSpan(18, 0, 0)) ||
                (time >= new TimeSpan(20, 0,0)) && (time <= new TimeSpan(21, 0,0)))
            {
                total = total + (0.2 * total);
            }
            if(total<5.50)
            {
                total = 5.50;
            }
            label6.Text = "Total: $" + Math.Round(total, 2);
            if (rad== 1)
            {
                label1.Text = "Ryde pool is confirmed";
            }
            else if (rad == 2)
            {
                label1.Text = " Ryde direct is confirmed";
            }
            label2.Text = "From: " + from;
            label3.Text = "To: " + to;
            label4.Text = "Booking Fee: 2.50";
            label5.Text = "Distance charge: " + fare;
            label7.Text = "Service Fees: 1.75";
            label6.Text = "Total: " + total;

        }
    }
}
