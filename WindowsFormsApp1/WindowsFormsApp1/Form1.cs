﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string from = textBox1.Text;
            string to = textBox2.Text;
            double distance = 0;
            int rad = 0;
            int flag = 0;
            if (from == "" || to == "")
            {
                MessageBox.Show("Please enter both locations");
                flag = 1;
            }
            else if (radioButton1.Checked == false && radioButton2.Checked == false)
            {
                MessageBox.Show("Please choose type of ride");
                flag = 1;
            }
            else if (from == "275 Yorkland Blvd" && to =="CN Tower")
            {
                distance = 22.9;
            }
            else if(from == "Fairview Mall" && to == "Tim Hortons")
                {
                distance = 1.2;
            }
            else
            {
                flag = 1;
                MessageBox.Show("Please enter valid locations");
            }
            if(radioButton1.Checked == true)
            {
                rad = 1;
            }else if (radioButton2.Checked == true)
            {
                rad = 2;
            }
            if (flag == 0)
            {
                Form2 screen2 = new Form2(distance, from, to, rad);
                screen2.ShowDialog();
            }
        }
    }
}
